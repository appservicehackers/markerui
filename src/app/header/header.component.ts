import { Component, OnInit } from '@angular/core';
import { RouterOutlet, Router } from '@angular/router';

import { IncidentService } from "../incident-marker/incident.service";

import { snIncident } from "../models";

import { environment } from "../../environments/environment";
import { EmitterService, Toast } from "../shared/";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private service: IncidentService, private router: Router) { }

  ngOnInit() {
  }

  isDirty(){
    return this.service.isDirty();
  }

  applyAutoSave(e:Event){
    let name = this.service.getCurrentFile().name;
    name = name.replace('.txt','.json')
    EmitterService.broadcastCommand(this, "AutoSave", name);  
  }

  onFileSave(e: any) {
    let name = this.currentFileName;
    EmitterService.broadcastCommand(this, "FileSave", name);
  }

  onOpenOrImport(file:File){
    let name = file.name.toLowerCase()
    if ( name.includes('.json')) {
      this.service.setCurrentFile(file);
      EmitterService.broadcastCommand(this, "FileOpen", file);
    }
    // if ( name.includes('.txt')) {
    //   this.parser.setCurrentFile(file);
    //   EmitterService.broadcastCommand(this, "ImportCase", file);
    // }
  }

  onFileOpen(e: any) {
    this.router.navigate(['/trainer']);

    let file = e.target.files[0];
    this.onOpenOrImport(file);
  }


  get currentFileName(){
    let file = this.service.getCurrentFile();
    return file ? file.name : "";
  }
}
