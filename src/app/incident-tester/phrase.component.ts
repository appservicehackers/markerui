import { Component, OnInit, Input } from '@angular/core';

import { EmitterService } from "../shared";

import { LaSentence, snIncident } from "../models";

@Component({
  selector: 'app-phrase',
  templateUrl: './phrase.component.html',
  styleUrls: ['./phrase.component.css']
})
export class PhraseComponent implements OnInit {
  @Input() sentence: LaSentence;
  @Input() incident: snIncident;
  
  constructor() { }

  ngOnInit() {
  }

}
