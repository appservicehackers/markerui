import { Component, OnInit, Input } from '@angular/core';

import { snIncident, LaSentence } from "../models";

import { PredictionService } from "./prediction.service";

@Component({
  selector: 'app-prediction',
  templateUrl: './prediction.component.html',
  styleUrls: ['./prediction.component.css']
})
export class PredictionComponent implements OnInit {
  @Input() incident: snIncident

  constructor(private pService:PredictionService) { }

  ngOnInit() {
    this.pService.getRandomPrediction$().subscribe(result => {
      this.incident.severityPredict = `${result}`
    })
  }

    
  getSentences():Array<LaSentence>{
    return this.incident ? this.incident.sentences : [];
  }

  predictClass(){
    if ( this.incident.severity == this.incident.severityPredict ) {
      return 'badge-success'
    }
    return 'badge-danger'
  }

}
