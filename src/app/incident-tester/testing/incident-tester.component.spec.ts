import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentTesterComponent } from '../incident-tester.component';

describe('IncidentTesterComponent', () => {
  let component: IncidentTesterComponent;
  let fixture: ComponentFixture<IncidentTesterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentTesterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentTesterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
