import { Injectable } from "@angular/core";
import { Toast, EmitterService } from "../shared";
import { HttpClient } from "@angular/common/http";

import { snIncident, snTrainingSet, LaSentence } from "../models";

import { Observable, of, Subject, ReplaySubject } from "rxjs";
import { map, catchError } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  API_URL: string = `../../assets/data/`;


  constructor(private http: HttpClient) {
  }

  public getRandomPrediction$(): Observable<Number> {

    let data = 1 + Math.floor(Math.random() * 5);
    let result = new Subject<Number>();
    setTimeout(_ => {
      result.next(data);
    }, 10)
    
    return result;
  }

  public getIncidentPrediction$(): Observable<number> {
    const filename = "incidents.json";
    const url = `${this.API_URL}${filename}`;

    return this.http.get<number>(url).pipe(
      map(res => {
        return 1 + Math.floor(Math.random() * 5);
      }),
      catchError(error => {
        const msg = JSON.stringify(error, undefined, 3);
        Toast.error(error.message, url);
        return of<any>(msg);
      })
    );
  }
}
