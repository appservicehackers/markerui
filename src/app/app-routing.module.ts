import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncidentMarkerComponent } from './incident-marker/incident-marker.component';
import { IncidentTesterComponent } from './incident-tester/incident-tester.component';

const routes: Routes = [
  { path: 'trainer',  component: IncidentMarkerComponent },
  { path: 'tester',  component: IncidentTesterComponent },
  { path: '',   redirectTo: '/trainer', pathMatch: 'full' },
  { path: '**', component: IncidentMarkerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
