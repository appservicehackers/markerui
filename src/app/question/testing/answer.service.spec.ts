import { async, TestBed, getTestBed } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

import { AnswerService } from '../answer.service';

describe('AnswerService', () => {
  
  let injector: TestBed;
  let service: AnswerService;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
    })
    .compileComponents();

    injector = getTestBed();
    service = injector.get(AnswerService);
    httpMock = injector.get(HttpTestingController);
  }));

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
     expect(service).toBeTruthy();
  });


  it("should post the answer questions", () => {

    const answer = {
    };
    service.API_URL = 'myservice'

    service._postAnswer$(answer).subscribe(res => {
      expect(res).toBeTruthy()
    });

    const req = httpMock.expectOne(service.API_URL);
    expect(req.request.method).toBe("POST");
    req.flush({
      payload: []
    });
  });

  it("should catch error when post the answer questions", () => {

    const answer = {
    };
    service.API_URL = 'myservice'

    service._postAnswer$(answer).subscribe(res => {
    }, err => {
      console.log('returned and error as expected')
      expect(err).toBeTruthy()
    });

    const req = httpMock.expectOne(service.API_URL);
    expect(req.request.method).toBe("POST");
    req.flush({});
  });

});
