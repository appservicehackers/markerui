
import { qAtom } from "./q-atom";
import { LaSentence } from "./la-sentence";


export interface IsnIncident {
  number:string;
  category:string;
  priority:string;
  severity:string;
  short_description:string;
  description:string;
}

export class snIncident extends qAtom implements IsnIncident {

  number:string;
  category:string;
  title:string;
  
  priority:string;
  severity:string;

  severityPredict:string ='3'

  short_description:string;
  description:string;

  private lookup = {};
  sentences: Array<LaSentence> = new Array<LaSentence>();
  
  constructor(properties?: any) {
    super(properties)

    this.title = this.short_description;
    delete this.short_description;

    this.sentences.push(new LaSentence({
      text: this.description,
      incidentNumber: this.number,
    }));

    delete this.description;
  }


  applyPrediction(threshold: number){
    this.sentences.forEach(item => {
      item.applyPrediction(threshold)
  })
  }

}
