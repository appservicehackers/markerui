import { qAtom } from "./q-atom";
import { LaAttributionRelation } from "./la-attributionRelation";



export class LaSentence extends qAtom {

  text: string;
  incidentNumber: string;

  predictions: any;
  attributions: Array<LaAttributionRelation>;


  constructor(properties?: any) {
    let attributions = properties.attributions;
    delete properties.attributions;

    super(properties);
  }


  deleteEmptyAttributions() {
    let list = this.attributions && this.attributions.splice(0)
    this.attributions = list && list.filter(item => {
      return item.isSaveWorthy();
    });
    return this.attributions;
  }



  bold(name: string) {
    return `<b>${name}</b>`
  }

  replaceBold(text: string, name: string) {
    return text.replace(name, this.bold(name));
  }

  textMarkup(): string {
    let text = "&nbsp; &nbsp;" + this.text;
    if (this.hasAttributionRelation()) {
      this.attributions.forEach(attribute => {
        text = this.replaceBold(text, attribute.severityText);
        text = this.replaceBold(text, attribute.priorityText);
      })
    }
    return text;
  }



  clearPrediction() {
    this.predictions = undefined;
  }

  hasPrediction() {
    return this.predictions !== undefined
  }



  get predictedValue() {
    let value = this.predictions[0].value
    return value;
  }

  setPrediction(item, threshold = 100) {
    const keys = Object.keys(item.predictions);

    let list = []
    keys.forEach(key => {
      let data = 10000 * parseFloat(item.predictions[key]);
      let obj = {
        name: key,
        value: Math.round(data) / 100
      }
      list.push(obj)
    })

    this.predictions = list.sort((a, b) => b.value - a.value);
    this.applyPrediction(threshold)
  }

  applyPrediction(threshold: number) {
    if ( this.hasPrediction()) {
      let best = this.predictions[0];
      if (best.value >= threshold) {
        //this.rhetClass = best.name;
      }
    }
  }



  priorityColor() {
    // let result = this.getFindingAttribution();
    // if (result) {
    //   return result.priorityColor;
    // }
    return 'white';
  }



  hasAttributionRelation() {
    return this.attributions && this.attributions.length > 0;
  }

  getCurrentAttributionRelation() {
    return this.attributions && this.attributions[0];
  }

  addAttributionRelation(obj: LaAttributionRelation) {
    if (!this.hasAttributionRelation()) {
      this.attributions = new Array<LaAttributionRelation>();
    }
    this.attributions.push(obj);
    return this;
  }

  removeAttributionRelation(obj: LaAttributionRelation) {
    if (!this.hasAttributionRelation()) {
      return;
    }
    let index = this.attributions.indexOf(obj);
    if (index > -1) {
      this.attributions.splice(index, 1);
    }
    return this;
  }


  mergeTextFrom(sentence: LaSentence, extra: string = ' ') {
    this.text += `${extra}${sentence.text}`
    return this;
  }


}
