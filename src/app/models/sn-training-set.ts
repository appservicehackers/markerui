import { qAtom } from "./q-atom";

import { snIncident } from "./sn-incident";
import { isNgTemplate } from '@angular/compiler';


export class snTrainingSet extends qAtom {

    incidentNumber: string;
    text: string = "";

    private incidentLookup = {};
    incidentList: Array<snIncident>;


    constructor(properties?: any) {
        super(properties);
    }


    addMembers(list: Array<snIncident>){
        this.incidentList = list;
    }


    doApplyPrediction(threshold: number) {
        this.incidentList.forEach(item => {
            item.applyPrediction(threshold)
        })
    }



}
