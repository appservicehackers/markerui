import { qAtom } from "./q-atom";

export class LaAttributionRelation extends qAtom {

  priorityText: string;
  severityText: string;

  priorityValue:string;
  severityValue:string;
  


  constructor(properties?: any) {
    super(properties);
  }

  private isStringEmpty(item) {
    return item == null || item == "";
  }


  get priorityColor() {
    let result = this.priorityText;
    if( result === 'positive') {
      return `rgba(0,204,0,0.3)`;
    }
    else if( result === 'negative') {
      return `rgba(255,0,0,0.3)`;
    }
    return 'white';
  }


  get severityColor() {
    let result = this.severityText;
    if( result === 'positive') {
      return `rgba(0,204,0,0.3)`;
    }
    else if( result === 'negative') {
      return `rgba(255,0,0,0.3)`;
    }
    return 'white';
  }


  clear() {
    this.severityText = "";
    this.priorityText = "";
  }


  isSaveWorthy(){
    return !this.isEmpty();
  }

  isDeletable(){
    return this.isEmpty();
  }

  isEmpty() {
    return (
      this.isStringEmpty(this.severityText) &&
      this.isStringEmpty(this.priorityText) 
    );
  }

  isMissingData() {
    return (
      this.isStringEmpty(this.severityText) ||
      this.isStringEmpty(this.priorityText)
    );
  }
}
