import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";


import { ToastrModule } from 'ngx-toastr';

import { MatButtonModule } from "@angular/material/button";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import {MatSelectModule} from '@angular/material/select';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { QuestionComponent } from "./question/question.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { IncidentMarkerComponent } from "./incident-marker/incident-marker.component";
import { IncidentComponent } from "./incident-marker/incident.component";
import { AttributionComponent } from './incident-marker/attribution.component';
import { IncidentTesterComponent } from './incident-tester/incident-tester.component';
import { SentenceComponent } from './incident-marker/sentence.component';
import { PredictionComponent } from './incident-tester/prediction.component';
import { PhraseComponent } from './incident-tester/phrase.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    QuestionComponent,
    HeaderComponent,
    FooterComponent,
    IncidentMarkerComponent,
    IncidentComponent,
    AttributionComponent,
    IncidentTesterComponent,
    SentenceComponent,
    PredictionComponent,
    PhraseComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
    }), 
    NgbModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatGridListModule,
    MatSelectModule,
    AppRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
