import { Injectable } from "@angular/core";
import { Toast, EmitterService } from "../shared";
import { HttpClient } from "@angular/common/http";

import { snIncident, snTrainingSet, LaSentence } from "../models";

import { saveAs } from "file-saver";
import { Observable, of, Subject, ReplaySubject } from "rxjs";
import { map, catchError } from "rxjs/operators";


export const attributionRoles = [
  "Evidence",
  "Finding",
  "LegalRule",
  "Citation",
  "Reasoning",
];

export const priorityRoles = [
  "1",
  "2",
  "3",
  "4",
  "5",
];

export const severityRoles = [
  "1",
  "2",
  "3",
  "4",
  "5",
]

interface iIncidentData {
  records: [];
}

@Injectable({
  providedIn: "root"
})
export class IncidentService {
  API_URL: string = `../../assets/data/`;

  private currentFile: File;
  selectedText: string = "";
  private isCaseDirty: boolean = false;
  private modelStream$: Subject<any>;

  currentTrainingSet: snTrainingSet;

  constructor(private http: HttpClient) {
    EmitterService.registerCommand(this, "FileOpen", this.onFileOpen);
    EmitterService.registerCommand(this, "FileSave", this.onFileSave);
    EmitterService.registerCommand(this, "AutoSave", this.onAutoSave);
    EmitterService.processCommands(this);
  }

  getAttributionRoles(): string[] {
    return attributionRoles.slice();
  }

  getpriorityValues():string[] {
    return priorityRoles.slice();
  }

  
  getSeverityValues():string[] {
    return severityRoles.slice();
  }

  setCurrentFile(file: File) {
    this.currentFile = file;
  }

  getCurrentFile(): File {
    return this.currentFile;
  }

  public isDirty() {
    return this.isCaseDirty;
  }

  public markAsDirty() {
    this.isCaseDirty = true;
  }

  public markAsSaved() {
    this.isCaseDirty = false;
  }

  onFileOpen(file: File) {
    this.currentFile = file;
    Toast.info("opening...", this.currentFile.name);
    this.readAndRestoreFile(this.currentFile);
  }

  onFileSave(name) {
    Toast.info("saving...", name);
    let rename = name.replace(".txt", ".json");
    this.saveCaseAs(rename);
  }

  onAutoSave(name) {
    if (this.isDirty() && this.currentFile) {
      let filename = name ? name : this.currentFile.name;
      Toast.success("auto saving...", filename);
      this.saveCaseAs(filename);
    }
  }

  readAndRestoreFile(file: File) {
    var reader = new FileReader();
    reader.onerror = event => {
      Toast.error("fail...", JSON.stringify(event.target));
    };
    reader.onload = () => {
      let data = JSON.parse(reader.result as string);

      let model = this.createTrainingSet(data);
      this.getCurrentTrainingSet$().next(model);

      Toast.success("loading!", file.name);
    };
    reader.readAsText(file);
  }

  isArray(obj) {
    if (Array.isArray) {
      return Array.isArray(obj);
    }
    return Object.prototype.toString.call(obj) === "[object Array]"
      ? true
      : false;
  }

  saveCaseAs(name) {
    let model = this.currentTrainingSet; //.asJson()

    let data = JSON.stringify(model);

    const blob = new Blob([data], { type: "text/plain;charset=utf-8" });
    saveAs(blob, name);
    this.markAsSaved();
  }

  public getCurrentTrainingSet$(): Subject<snTrainingSet> {
    if (this.modelStream$ == null) {
      this.modelStream$ = new ReplaySubject<snTrainingSet>(2);
    }
    return this.modelStream$;
  }

  private createTrainingSet(data: any): snTrainingSet {
    this.currentTrainingSet = new snTrainingSet({});
    return this.currentTrainingSet;
  }

  private createModel(incidents: Array<any>): snTrainingSet {
    this.currentTrainingSet = new snTrainingSet({});

    const list = new Array<snIncident>();

    incidents.forEach(q => {
      list.push(
        new snIncident({
          number: q.number,
          category: q.category,
          priority: q.priority,
          severity: q.severity,
          short_description: q.short_description,
          description: q.description,
        })
      );
    });

    this.currentTrainingSet.addMembers(list);

    this.markAsSaved();

    return this.currentTrainingSet;
  }

  public getSampleIncidents$(): Observable<any> {
    const filename = "incidents.json";
    const url = `${this.API_URL}${filename}`;

    return this.http.get<iIncidentData>(url).pipe(
      map(res => {
        this.setCurrentFile(new File([], filename));
        let model = this.createModel(res.records);
        this.getCurrentTrainingSet$().next(model);
        return filename;
      }),
      catchError(error => {
        const msg = JSON.stringify(error, undefined, 3);
        Toast.error(error.message, url);
        return of<any>(msg);
      })
    );
  }
}
