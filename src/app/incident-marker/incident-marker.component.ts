import { Component, OnInit, OnDestroy } from "@angular/core";
import { Toast, EmitterService, SubSink } from "../shared";

import { IncidentService } from "./incident.service";

import { snTrainingSet, snIncident } from "../models";

import { environment } from "../../environments/environment";

//https://css-tricks.com/snippets/css/a-guide-to-flexbox/

@Component({
  selector: "app-incident-marker",
  templateUrl: "./incident-marker.component.html",
  styleUrls: ["./incident-marker.component.css"]
})
export class IncidentMarkerComponent implements OnInit, OnDestroy {
  sub: SubSink = new SubSink();
  currentTrainingSet: snTrainingSet;

  constructor(private iService: IncidentService) {}

  ngOnInit() {
    this.sub.add(this.iService.getCurrentTrainingSet$().subscribe( model => {
      this.currentTrainingSet = model;
    }))

    this.sub.add(this.iService.getSampleIncidents$().subscribe(filename => {
      Toast.success("loaded!", filename);
    }));

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getIncidents():Array<snIncident>{
    return this.currentTrainingSet ? this.currentTrainingSet.incidentList : [];
  }
}
