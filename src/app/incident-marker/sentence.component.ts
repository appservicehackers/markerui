import { Component, OnInit, Input } from '@angular/core';

import { EmitterService } from "../shared";

import { LaSentence, snIncident } from "../models";

@Component({
  selector: 'app-sentence',
  templateUrl: './sentence.component.html',
  styleUrls: ['./sentence.component.css']
})
export class SentenceComponent implements OnInit {
  @Input() sentence: LaSentence;
  @Input() incident: snIncident;
  selected:boolean = false;

  constructor() { }

  ngOnInit() {
    EmitterService.registerCommand(this,"CloseAll",this.doClose);
    //EmitterService.registerCommand(this,"Predict",this.doPrediction);
    EmitterService.processCommands(this);
  }

  
  doClose() {
    this.selected = false;
  }


  doOpen() {
    if ( this.selected) {
     // this.doClose()
    }
    else {
      EmitterService.broadcastCommand(this, "CloseAll", null, _ => {
        this.selected = true;
      });
  
      //EmitterService.broadcastCommand(this, "RefreshSelected");
    }
  }

  isSelected(){
    return this.selected;
  }

}
