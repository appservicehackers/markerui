import { Component, OnInit, Input, TemplateRef } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";

import { IncidentService } from "./incident.service";

import { LaSentence, snIncident } from "../models";
import { LaAttributionRelation } from "../models";
import { EmitterService } from "../shared";

@Component({
  selector: "app-attribution",
  templateUrl: "./attribution.component.html",
  styleUrls: ["./attribution.component.css"]
})
export class AttributionComponent implements OnInit {
  @Input() sentence: LaSentence;
  @Input() incident: snIncident;
  attribution: LaAttributionRelation;
  formData: FormGroup;

  constructor(
    private builder: FormBuilder,
    private iService: IncidentService
  ) {}

    // convenience getter for easy access to form fields
  get f() { return this.formData.controls; }

  ngOnInit() {
    if (this.sentence.hasAttributionRelation()) {
      this.attribution = this.sentence.getCurrentAttributionRelation();
    } else {
      this.attributionAdd();
    }

    this.formData = this.builder.group({
     // priorityText: [this.attribution.priorityText], 
     // priorityValue: [this.attribution.priorityValue], 
      severityText: [this.attribution.severityText], 
      severityValue: [this.attribution.severityValue], 
    });
  }

  attributionAdd() {
    this.iService.markAsDirty();
    this.attribution = new LaAttributionRelation({
      priorityValue: this.incident.priority,
      severityValue: this.incident.severity,
    });
    this.sentence.addAttributionRelation(this.attribution);
  }

  getSelectedText() {
    var text = "";
    if (window.getSelection) {
      text = window.getSelection().toString();
    }
    return text;
  }

  isCurrentAttribution(obj) {
    return this.attribution === obj;
  }

  setCurrentAttribution(obj) {
    this.attribution = obj;
  }

  itemClear(obj) {
    this.attribution = obj;
    this.attribution.clear();
  }

  getSeverityValues(): string[] {
    let roles = this.iService.getSeverityValues();
    return roles;
  }

  itemSeverity(obj) {
    this.attribution = obj;
    this.attributionSeverity();
  }
  attributionSeverity() {
    this.iService.markAsDirty();
    this.f['severityText'].setValue( this.getSelectedText());
  }

  selectSeverityValue(obj, value: string): void {
    this.iService.markAsDirty();
    this.attribution = obj;
    this.f['severityValue'].setValue(value);
  }

  severityChanged(event$){
    this.iService.markAsDirty();
  }

  getpriorityValues(): string[] {
    let roles = this.iService.getpriorityValues();
    return roles;
  }

  itemPriority(obj) {
    this.attribution = obj;
    this.attributionpriority();
  }
  attributionpriority() {
    this.iService.markAsDirty();
    this.f['priorityText'].setValue( this.getSelectedText());
  }

  selectpriorityValue(obj, value: string): void {
    this.iService.markAsDirty();
    this.attribution = obj;
    this.f['priorityValue'].setValue(value);
  }

  priorityChanged(event$){
    this.iService.markAsDirty();
  }

  setAttribteValues(){
    this.attribution.severityText = this.f['severityText'].value;
    this.attribution.severityValue = this.f['severityValue'].value;
    //this.attribution.priorityText = this.f['priorityText'].value;
    //this.attribution.priorityValue = this.f['priorityValue'].value;
  }

  attributionDone() {
    this.setAttribteValues();
    this.sentence.deleteEmptyAttributions();
    EmitterService.broadcastCommand(this, "CloseAll");
    EmitterService.broadcastCommand(this, "AutoSave");
  }

  doClose() {
    this.setAttribteValues();
    this.sentence.deleteEmptyAttributions();
    EmitterService.broadcastCommand(this, "CloseAll");
  }
}
