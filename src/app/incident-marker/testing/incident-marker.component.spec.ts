import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentMarkerComponent } from '../incident-marker.component';

describe('IncidentMarkerComponent', () => {
  let component: IncidentMarkerComponent;
  let fixture: ComponentFixture<IncidentMarkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentMarkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentMarkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
