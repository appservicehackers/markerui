import { Component, OnInit, Input } from '@angular/core';

import { snIncident, LaSentence } from "../models";

@Component({
  selector: 'app-incident',
  templateUrl: './incident.component.html',
  styleUrls: ['./incident.component.css']
})
export class IncidentComponent implements OnInit {
  @Input() incident: snIncident
  
  constructor() { }

  ngOnInit() {
  }

  
  getSentences():Array<LaSentence>{
    return this.incident ? this.incident.sentences : [];
  }


}
